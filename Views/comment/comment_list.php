<h1>Коментарии</h1>
<div>

    <?php foreach ($comments as $comment): ?>
    <div class="container" style="margin: auto;">
        <div class="col-sm-1">
            <?php $user = \Blog\Models\User::get($comment->getEmailUser()); ?>
                <a href="?c=user&a=profile&email=<?php echo $user->getEmail(); ?>">
                    <img class="img-rounded img-responsive"
                         style="width: 100px; height: 100px;"
                         src="\Blog\<?php echo $user->getAvatarPath();?>" />
                </a>

            <div>
                <?php echo $user->getFirstName() . " " . $user->getLastName(); ?>
            </div>
        </div>

        <div class="col-sm-4">
            <div> <?php echo $comment->getMessage(); ?></div>
            <div> <?php echo $comment->getDate(); ?></div>
        </div>


        <?php if(isset($_SESSION["USER"]) &&
                unserialize($_SESSION['USER'])->getEmail() == $comment->getEmailUser()): ?>
            <div class="col-sm-1">
                <a class="btn btn-link"
                   href="?c=pages&a=changeComment&id=<?php echo $comment->getId(); ?>">
                    Изменить
                </a>
                <a class="btn btn-link"
                   href="?c=comment&a=delete&id=<?php echo $comment->getId(); ?>">Удалить</a>
            </div>
        <?php endif; ?>
    </div>
    <?php endforeach; ?>
</div>