

<div class="" style="width: 20%; margin: auto;">
  <h1 style="text-align: center;">Смена пароля</h1>
  <form class="" action="?c=user&a=changePassword" method="post">

    <label for="current-password">Текущий пароль: </label>
    <div class="form-group">
      <input class="form-control" type="password"
       name="current-password" placeholder="Пароль">
    </div>

    <label for="password">Новый пароль: </label>
    <div class="form-group">
      <input class="form-control" type="password"
       name="password">
    </div>

    <label for="repeat-password">Повторите новый пароль: </label>
    <div class="form-group">
      <input class="form-control" type="password"
       name="confirm-password">
    </div>

    <div class="form-group">
      <button class="btn btn-default" type="submit" name="submit">Изменить пароль</button>
    </div>
  </form>
</div>
