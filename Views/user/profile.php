<h1 style="text-align:center;">Профиль</h1>
<div  style="width: 50%; margin:auto;" class="">

  <img class="img-rounded img-responsive"
       style="margin: auto;"
       width="200"
       height="200"
       src="/Blog/<?php echo $user->getAvatarPath(); ?>"
       alt="">

  <div class="" style="text-align:center;">
    <p><b>Имя:</b> <?php echo $user->getFirstName(); ?></p>
    <p><b>Фамилия:</b> <?php  echo $user->getLastName();?> </p>
    <p><b>Э-почта:</b> <?php  echo $user->getEmail(); ?> </p>
    <p><b>Дата рождения:</b> <?php echo $user->getBirthday(); ?> </p>
      <?php
        if(isset($_SESSION["USER"]) &&
            unserialize($_SESSION["USER"])->getEmail() == $user->getEmail()):
      ?>
            <a class="btn btn-link" href="?c=pages&a=changePassword">Изменить пароль</a>
      <?php endif; ?>
  </div>
</div>
