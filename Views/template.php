<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="/Blog/Views/styles/bootstrap.min.css" type="text/css">

  </head>
  <body>
    <header>
      <nav class="navbar navbar-default">
      <?php

        if(!isset($_SESSION["USER"])){
          require_once __DIR__ ."/guest_menu.php";
        } else {
          $user = unserialize($_SESSION["USER"]);
          require_once __DIR__ . "/user_menu.php";
        }
      ?>
      </nav>
    </header>






    <?php


      if(empty($controllers) && empty($action) ) {
          $controller = "pages";
          $action = "home";
      }


      try{
          \Blog\Router::run($controller, $action);
      } catch (\Exception $e) {
          header("HTTP/1.0 404 Not Found");
          echo "<h1>К сожалению запрашиваемая страница не найдена. :(</h1>";
      }

    ?>

  </body>
</html>
