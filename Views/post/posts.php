
<?php   foreach ($posts as $post): ?>

<div class="well" style="margin: 5% 10%;">
    <h1><a href="?c=post&a=getPost&id=<?php echo $post->getId(); ?>">
            <?php echo $post->getTitle(); ?></a></h1>
    <p><?php echo $post->getArticleReview(); ?>...</p>
    <div class=""> <p>Автор: <?php echo $post->getAuthor(); ?></p> </div>
    <div class=""><p>Дата: <?php echo $post->getDate(); ?></p></div>
    <?php if(isset($_SESSION["USER"]) &&
        unserialize($_SESSION['USER'])->isSuperUser()):
     ?>
        <div class="">
            <a class="btn btn-link"
               href="?c=post&a=delete&id=<?php
               echo $post->getId();
               ?>">Удалить
            </a>
        </div>
        <div class="">
            <a class="btn btn-link"
               href="?c=pages&a=changePost&id=<?php
               echo $post->getId();
               ?>">Изменить
            </a>
        </div>
    <?php endif;?>
</div>

<?php endforeach; ?>
<div style="text-align: center">
    <div class="pagination">
        <?php for($i = 1; $i <= $countPage; $i++): ?>
            <li><a href="?c=post&a=getPage&page=<?php echo $i ?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </div>
</div>