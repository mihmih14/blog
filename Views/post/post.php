<div class="well" style="margin: 5% 10%;">
    <h1> <?php echo $post->getTitle(); ?> </h1>
    <p> <?php echo $post->getArticle(); ?></p>
    <div class=""> <p>Автор: <?php echo $post->getAuthor(); ?></p> </div>
    <div class=""><p>Дата: <?php echo $post->getDate(); ?></p></div>
    <?php if(isset($_SESSION["USER"]) &&
        unserialize($_SESSION['USER'])->isSuperUser()):
        ?>
        <div class="">
            <a class="btn btn-link"
               href="?c=post&a=delete&id=<?php
               echo $post->getId();
               ?>">Удалить
            </a>
        </div>
        <div class="">
            <a class="btn btn-link"
               href="?c=pages&a=changePost&id=<?php
               echo $post->getId();
               ?>">Изменить
            </a>
        </div>
    <?php endif;?>
</div>