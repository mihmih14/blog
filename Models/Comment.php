<?php

namespace Blog\Models;
use Blog\DataBase;
use Blog\Models\Interfaces\SenderInterface;

class Comment implements SenderInterface
{
    private $id;

    private $emailUser;

    private $idPost;

    private $message;

    private $date;


    public function __construct($id, $emailUser, $idPost, $message, $date)
    {
        $this->id = $id;
        $this->emailUser = $emailUser;
        $this->idPost = $idPost;
        $this->message = $message;
        $this->date = $date;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getEmailUser()
    {
        return $this->emailUser;
    }


    public function getIdPost()
    {
        return $this->idPost;
    }


    public function getMessage()
    {
        return $this->message;
    }


    public function getDate()
    {
        return $this->date;
    }

    public static function findComment($id)
    {
        $db = DataBase::getInstance();
        $sql = "SELECT * FROM Comment WHERE Id=".$id;

        $row = $db->query($sql)->fetchAll();

        return new Comment($row[0]['Id'], $row[0]['EmailUser'],
                           $row[0]['IdPost'], $row[0]['Message'],
                           $row[0]['DateComment']);
    }

    public static function findCommentsOfPost($idPost)
    {
        $db = DataBase::getInstance();

        $sql = $sql = "SELECT * FROM Comment WHERE IdPost=".$idPost;

        $comments = [];

        foreach ($db->query($sql) as $row) {
            $comments[] = new Comment($row['Id'], $row['EmailUser'],
                                      $row['IdPost'], $row['Message'],
                                      $row['DateComment']);
        }


        return $comments;
    }

    public static function write($email, $post)
    {
        $db = DataBase::getInstance();
        $query = $db->prepare("INSERT INTO Comment(EmailUser, IdPost, Message, DateComment)
                Values(:email, :idPost, :msg, NOW())");
        $values = [":email" => $email,
                   ":idPost" => $post['id'],
                   ":msg" => $post["msg"]];
        $success = $query->execute($values);

        return $success;
    }

    public static function modify($id, $message)
    {
        $db = DataBase::getInstance();
        $sql = "UPDATE Comment SET Message=:msg WHERE Id=:id";
        $query = $db->prepare($sql);
        $success = $query->execute([':msg' => $message, 'id' => $id] );

        return $success;
    }

    public static function delete($id)
    {
        $db = DataBase::getInstance();
        $query = $db->prepare("DELETE FROM Comment WHERE Id=:id");
        $success = $query->execute([':id' => $id]);

        return $success;
    }

}
