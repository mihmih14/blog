<?php

namespace Blog\Models;

use \Blog\Models\Comment;
use \Blog\Models\Post;


class Pages
{

    public static function login()
    {
        require_once __DIR__ . '/../Views/authorization/session.php';
    }

    public static function logout()
    {
        require_once __DIR__ . '/../Views/authorization/registration.php';
    }

    public static function post()
    {
        require_once __DIR__ . '/../Views/post/post_form.php';
    }

    public static function home()
    {
        require_once __DIR__ . "/../Views/home.php";
    }

    public static function changePassword()
    {
        require_once __DIR__ . "/../Views/user/change_password.php";
    }

    public static function changePost($id)
    {
        $post = Post::getPost($id);
        require_once __DIR__ . "/../Views/post/change_post.php";
    }

    public static function changeComment($id)
    {
        $comment = Comment::findComment($id);
        require_once __DIR__ . "/../Views/comment/comment_change.php";
    }
}
