<?php

class NotFoundDataException extends Exception
{
    protected $message = "Data is not found";
}
