<?php

class InvalidContactException extends Exception
{
    protected $message = "Invalid firstName or lastname";
}
