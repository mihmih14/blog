<?php

namespace Blog\Models;

use \Blog\Models\User;
use \Blog\Models\SuperUser;
use \Blog\DataBase;

class Authorization
{


    public static function signIn($email, $password) {

        $db = DataBase::getInstance();


        $result = $db->query("SELECT *
                              FROM User
                              Where Email = '".$email."'");



        $data = $result->fetch(\PDO::FETCH_ASSOC);

        if(!password_verify($password, $data['Password'])) {
            return false;
        }


        $user = new User($data['FirstName'], $data['LastName'], $data['Birthday'],
                        $data['Avatar'], $data['Email'], $data['Password']);

        $superUser = SuperUser::get($user->getEmail());
        
        if (!empty($superUser)) {
            $superUser = new SuperUser($user, $superUser["Id"]);
            $_SESSION['USER'] = serialize($superUser);
        } else {
            $_SESSION['USER'] = serialize($user);
        }

        return true;
    }



    public static function singOut($user)
    {
        $db = DataBase::getInstance();

        self::checkUser($user);

        $sql = "INSERT INTO User(
               Email, FirstName,
               LastName, Birthday,
               Password, avatar)
               Values(:email, :fname,
               :lname, :birthday,
               :password, :avatar );";

        $password = password_hash($user->getPassword(), PASSWORD_DEFAULT);

        $query = $db->prepare($sql);
        $success = $query->execute([ "email" => $user->getEmail(),
                                     "fname" =>  $user->getFirstName(),
                                     "lname" => $user->getLastName(),
                                     "birthday" => $user->getBirthday() ,
                                     "password" => $password ,
                                     "avatar" => $user->getAvatarPath() ]);

        if(!$success) {
            echo "Ошибка при регистрации";
        }

        return true;
    }



    private static function checkUser($user)
    {
        if ($user->getFirstName() == "" || $user->getLastName() == "") {
            require_once $_SERVER["DOCUMENT_ROOT"] . "/Blog/Models/Exception/InvalidContactException.php";
            throw new InvalidContactException();
        }

        if ($user->getPassword() == "" || strlen($user->getPassword()) < 8) {
            require_once $_SERVER["DOCUMENT_ROOT"] . "/Blog/Models/Exception/InvalidPasswordException.php";
            throw new InvalidPasswordException();
        }

        if (!filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)) {
            require_once $_SERVER["DOCUMENT_ROOT"] . "/Blog/Models/Exception/InvalidEmailException.php";
            throw new InvalidEmailException();
        }

        if($user->getBirthday() == "") {
            require_once $_SERVER["DOCUMENT_ROOT"] . "/Blog/Exception/InvalidBirthdayException.php";
            throw new InvalidBirthdayException();
        }

    }

}
