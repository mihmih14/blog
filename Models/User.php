<?php

namespace Blog\Models;

use Blog\Models\Interfaces\SuperUserInterface;
use Blog\DataBase;

class User extends Human implements SuperUserInterface
{

    private $avatar;

    private $email;

    private $password;

    public function __construct($firstName, $lastName,
                                $birthday, $avatar,
                                $email, $password)
    {
        parent::__construct($firstName, $lastName, $birthday);
        $this->avatar = $avatar;
        $this->email  = $email;
        $this->password = $password;
    }

    public function getAvatarPath()
    {
        return $this->avatar;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function isSuperUser()
    {
        return false;
    }


    public static function changePassword($email, $password, $currentPassword)
    {
        $db = DataBase::getInstance();

        if(self::checkPassword($password)) {
            $user = self::get($email);
  
            if(password_verify($currentPassword, $user->getPassword())) {
                $password = password_hash($password, PASSWORD_DEFAULT);

                $query = $db->prepare("UPDATE User
                              SET Password = :password
                              WHERE Email = :email");
                $query->bindParam(':password', $password, PDO::PARAM_STR);
                $query->bindParam(':email', $email, PDO::PARAM_STR);
                return $query->execute();
            }
        }

        return false;
    }

    public static function exit()
    {
        session_destroy();
        header("Location: " . "?c=post&a=getPage&page=1");
    }

    public static function get($email)
    {
        $db = DataBase::getInstance();
        $query = $db->prepare("SELECT *
                        FROM User
                        WHERE Email = :email");

        $query->execute([':email' => $email]);

        $row = $query->fetch();

        return new User($row['FirstName'], $row['LastName'],
                        $row['Birthday'], $row['Avatar'],
                        $row['Email'], $row['Password']);
    }

    public static function checkInformation($user)
    {
        return !empty($user->getFirstName()) &&
            !empty($user->getLastName()) &&
            !empty($user->getEmail()) &&
            !empty($user->getPassword()) &&
            !empty($user->getBirthday());
    }

    private static function checkPassword($password)
    {
        if (strlen($password) < 8) {
            return false;
        }

        return true;
    }

}
