<?php

namespace Blog\Models;


class Human
{

    private $firstName;

    private $lastName;

    private $birthday;


    public function __construct($firstName, $lastName, $birthday)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthday = $birthday;
    }


    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }
}
