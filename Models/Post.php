<?php

namespace Blog\Models;

use Blog\Models\Interfaces\SenderInterface;
use Blog\DataBase;


class Post implements SenderInterface
{
    private $id;

    private $author;

    private $countsLikes;

    private $title;

    private $article;

    private $date;

    public function __construct($id, $author, $countsLikes,
                                $title, $article, $date)
    {
        $this->id = $id;
        $this->author = $author;
        $this->countsLikes = $countsLikes;
        $this->title = $title;
        $this->article =$article;
        $this->date = $date;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getCountLikes()
    {
        return $this->countsLikes;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getArticle()
    {
        return $this->article;
    }

    public function getArticleReview()
    {
        return substr($this->getArticle(), 0, 700);
    }

    public function getDate()
    {
        return $this->date;
    }

    public static function like($idPost, $emailUser)
    {
        return false;
    }

    public static function cancelToLike($idPost, $emailUser)
    {
        return false;
    }

    public static function getPost($id)
    {
        $db = DataBase::getInstance();
        $sql = "SELECT Post.*, User.FirstName, User.LastName
              From Post
              INNER JOIN SuperUser
              ON Post.IdSuperUser = SuperUser.Id
              INNER JOIN User
                ON User.Email = SuperUser.EmailUser
                WHERE Post.Id ='".$id."'";
        $query = $db->query($sql);
        $row = $query->fetchAll();

        return new Post($row[0]['Id'], $row[0]['FirstName'].$row[0]["LastName"],
           $row[0]['CountLikes'], $row[0]["Title"], $row[0]["Article"], $row[0]["DatePost"]);
    }


    public static function getPosts()
    {
        $db = DataBase::getInstance();
        $sql = "SELECT Post.*, User.FirstName, User.LastName
                From Post
                INNER JOIN SuperUser
                   ON Post.IdSuperUser = SuperUser.Id
                INNER JOIN User
                   ON User.Email = SuperUser.EmailUser
                ORDER BY Post.DatePost DESC";

        $posts = [];

        foreach($db->query($sql) as $row ) {
            $posts[] = new Post($row['Id'], $row['FirstName']." ".$row["LastName"],
                              0, $row["Title"], $row["Article"], $row["DatePost"]);
        }

        return $posts;
    }

    public static function getPage($number)
    {

        if(intval($number) < 1) {
            throw new Exception("NaN");
        }

        $countRecords = 5;
        $db = DataBase::getInstance();
        $offset = 1 * ($number - 1) * $countRecords;
        $sql = "SELECT Post.*, User.FirstName, User.LastName
                 From Post
                 INNER JOIN SuperUser
                     ON Post.IdSuperUser = SuperUser.Id
                 INNER JOIN User
                     ON User.Email = SuperUser.EmailUser
                 ORDER BY Post.DatePost DESC
                 LIMIT $countRecords OFFSET $offset";

        $posts = [];

        foreach($db->query($sql) as $row) {
            $posts[] = new Post($row['Id'], $row['FirstName']." ".$row["LastName"],
                0, $row["Title"], $row["Article"], $row["DatePost"]);
        }

        return $posts;
    }

    public static function getCountPage()
    {
        $sql = "SELECT Count(id)
                FROM Post";
        $db = DataBase::getInstance();
        $query = $db->prepare($sql);
        $query->execute();
        return ceil($query->fetchColumn() / 5);
    }

    public static function write($idUser, $post)
    {
        $db = DataBase::getInstance();
        $query = $db->prepare("INSERT INTO Post(
                                IdSuperUser, CountLikes,
                                Title, Article, DatePost)
                                Values(:id, :likes, :title, :article, NOW())");
        $values = [":id" => $idUser,
                   ":likes" => 0,
                   ":title" => $post['title'],
                   ":article" => $post['article']];

        return $query->execute($values);
    }

    public static function modify($id, $post)
    {
        $db = DataBase::getInstance();
        $query = $db->prepare("UPDATE Post
                     SET Title = :title, Article = :article
                     WHERE Id = :id");
        $values = [":title" => $post['title'],
                   ":article" => $post['article'],
                   ":id" => $id];

        return $query->execute($values);
    }

    public static function delete($id)
    {
        $db = DataBase::getInstance();
        $query = $db->prepare("DELETE FROM Comment
                                        WHERE IdPost = :id;
                                        DELETE FROM Post
                                        WHERE Id = :id");
        return $query->execute([':id' => $id]);
    }

}
