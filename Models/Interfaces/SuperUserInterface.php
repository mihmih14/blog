<?php

namespace Blog\Models\Interfaces;

interface SuperUserInterface
{
    public function isSuperUser();
}
