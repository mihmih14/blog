<?php

namespace Blog\Models\Interfaces;

interface SenderInterface
{
    public static function write($id, $message);
    public static function modify($id, $message);
    public static function delete($id);
}
