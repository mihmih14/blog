<?php

namespace Blog\Models;
use Blog\DataBase;


class SuperUser extends User
{

    private $id;

    public function __construct($user, $id)
    {
        parent::__construct(
            $user->getFirstName(),
            $user->getLastName(),
            $user->getBirthday(),
            $user->getAvatarPath(),
            $user->getEmail(),
            $user->getPassword()
        );
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function isSuperUser()
    {
        return true;
    }

    public static function get($email)
    {
        $db = DataBase::getInstance();
        $query = $db->prepare("SELECT *
                                  FROM SuperUser
                                  WHERE EmailUser = :email");
        $query->execute([':email' => $email]);

        return $query->fetch();
    }
}
