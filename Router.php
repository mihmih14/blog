<?php

namespace Blog;

use \Blog\Controllers\AuthorizationController;
use \Blog\Controllers\CommentController;
use \Blog\Controllers\PagesController;
use \Blog\Controllers\PostController;
use \Blog\Controllers\UserController;

class Router {

  public static function run($controller, $action)
  {


      $controller = strtolower($controller) . "Controller";
      $controller[0] = strtoupper($controller[0]);
      $obj = "\\Blog\\Controllers\\" .$controller;

      if(!file_exists(__DIR__ . "/Controllers/" . $controller  . ".php")) {
          throw new \Exception("FileNotExist");
      }


      $c = new $obj();

      if (!method_exists($c, $action)) {
          throw new \Exception("MethodNotExist");
      }
      $c->{$action}();


  }
}

