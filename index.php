<?php
session_start();
spl_autoload_register();
spl_autoload_register(function ($class) {
    echo "Class = " . $class;
    $paths = explode("\\", $class);
    $className = "";

    for($i = 0; $i < count($paths); $i++) {
        echo $i . ") " . $paths[$i];
        if ($i == (count($paths) - 1)) {
            $className .= $paths[$i];
        } else {
            $className .= strtolower($paths[$i]) . "/";
        }
    }
    $path = __DIR__ . "/" . $className . ".php";
    echo " Path = " . $path;
    require_once $path;
});

if (isset($_GET['c']) && isset($_GET['a'])) {
    $controller = $_GET['c'];
    $action = $_GET['a'];
}

require_once __DIR__ .'/Blog/Views/template.php';