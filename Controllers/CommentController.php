<?php

namespace Blog\Controllers;

use \Blog\Models\Comment;

class CommentController
{

    public function getCommentsOfPost()
    {
        $idPost = $_GET['id'];
        $comments = Comment::findCommentsOfPost($idPost);
        require_once __DIR__ . "/../Views/comment/comment_list.php";
    }

    public function write()
    {
        if (isset($_POST['submit'])) {
            $emailUser = unserialize($_SESSION['USER'])->getEmail();
            $idPost = $_GET['idPost'];
            $message = $_POST["message"];

            $success = Comment::write($emailUser,
                                        ['id' => $idPost,
                                         'msg' => $message]);
            if ($success) {
                header("Location:"."?c=post&a=getPost&id=$idPost");
                $message = "Коментарий успешно добавлен";
                require_once __DIR__ . "/../Views/success.php";
            } else {
                $message = "Произошла проблема";
                echo $message;
            }
        }
    }

    public function modify()
    {
        if (isset($_POST['submit'])) {
            $id = $_GET['id'];
            $message = $_POST['message'];
            $success = Comment::modify($id, $message);

            if ($success) {
                $message = "Коментарий изменен";
                require_once __DIR__ . "/../Views/success.php";
            } else {
                $message = "Произошла ошибка";
                echo $message;
            }
        }
    }

    public function delete()
    {
        $id = $_GET['id'];
        Comment::delete($id);
    }

}
