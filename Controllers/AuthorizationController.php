<?php

namespace Blog\Controllers;

use \Blog\Models\Authorization;
use \Blog\Models\User;

class AuthorizationController
{

    public function login()
    {
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            if (Authorization::signIn($email, $password)) {
                header("Location: "."?c=post&a=getPage&page=1");
            } else {
                header("Location: "."?c=pages&a=login");
            }
        }
    }

    public function logout()
    {
        if(isset($_POST["submit"])) {
            $firstName = $_POST["first-name"];
            $lastName = $_POST["last-name"];
            $email = $_POST["email"];
            $birthday = $_POST["birthday"];
            $password = $_POST["password"];
            $avatar = "/../Views/styles/images/cat.jpg";
            $confirmPassword = $_POST["confirm-password"];

            if($password != $confirmPassword) {
                require __DIR__ .'/Views/authorization/registration.php';
                echo "<div class='alert alert-danger'>
                <strong>Ошибка</strong> Вы ввели не правильно пароль.
                </div>";
                return;
            }

            $user = new User($firstName, $lastName,
                             $birthday, $avatar, $email,
                             $password);

            try {
                require_once $_SERVER['DOCUMENT_ROOT'] . "/Models/Authorization.php";
                Authorization::singOut($user);
                header("Location: "."?c=post&a=showAll");
                require($_SERVER['DOCUMENT_ROOT'] . " /Views/authorization/success_reg.php");
            } catch (InvalidEmailException $e) {
                require $_SERVER["DOCUMENT_ROOT"] . " /Views/authorization/registration.php";
                echo "E-mail Введен не правильно!";
            } catch (InvalidContactException $e) {
                require $_SERVER["DOCUMENT_ROOT"] . " /Views/authorization/registration.php";
                echo "Имя или фамилия введена не правильно!";
            } catch (InvalidPasswordException $e) {
                require $_SERVER["DOCUMENT_ROOT"] . " /Views/authorization/registration.php";
                echo "Пароль введен не правильно!";
            } catch (InvalidBirthdayException $e) {
                require $_SERVER["DOCUMENT_ROOT"] . " /Views/authorization/registration.php";
                echo "Дата рождения введене не правильно!";
            }

        }
    }
}
