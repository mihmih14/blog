<?php

namespace Blog\Controllers;

use \Blog\Models\Post;
use \Blog\Models\Comment;

class PostController
{



    public function add()
    {
        if(isset($_POST['submit'])) {
            $user = unserialize($_SESSION['USER']);
            echo "ID USER = " . $user->getId();
            $id = $user->getId();
            $post = ['title' => $_POST['title'],
                     'article' => $_POST['article']];

            if(Post::write($id, $post)) {
                //success
                $message = "Запись успешно добавлена";
                require_once __DIR__ . "/../Views/success.php";
            } else {
                //unsuccess
                echo "<p>Запись не добавлена</p>";
            }

            require_once __DIR__ . "/../Views/post/post_form.php";
        }
    }

    public function modify()
    {
        if(isset($_POST['submit'])) {
            $id = $_GET['id'];
            $post = ['title' => $_POST['title'],
                     'article' => $_POST['article']];

            if(Post::modify($id, $post)) {
                $message = "Запись изменена";
                require_once __DIR__ . "/../Views/success.php";
            } else {
                $message = "При изменении записи произошла ошибка";
            }
        }
    }

    public function delete()
    {
        $id = $_GET['id'];
        if(Post::delete($id)) {
            //success
            $message = "Запись удалена";
            require_once __DIR__ . "/../Views/success.php";
        } else {
            $message = "При удалении произошла ошибка";
            echo $message;
        }
    }

    public function getPage()
    {
        $pageNumber = $_GET["page"];
        $posts = Post::getPage($pageNumber);
        $countPage = Post::getCountPage();
        if(count($posts) > 0) {
            require_once __DIR__ . "/../Views/post/posts.php";
        } else {
            echo "<h1>Записей пока нет.</h1>";
        }
    }

    public function getPost()
    {

        $id = $_GET["id"];
        $post = Post::getPost($id);
        $comments = Comment::findCommentsOfPost($id);
        require_once __DIR__ . "/../Views/post/post.php";

        if(count($comments) > 0) {
            require_once __DIR__ . "/../Views/comment/comment_list.php";
        } else {
            echo "<h1>коментарий пока нет.</h1>";
        }

        if (isset($_SESSION['USER'])) {
            require_once __DIR__ . "/../Views/comment/comment_form.php";
        }
    }
}
