<?php

namespace Blog\Controllers;

use \Blog\Models\Pages;

class PagesController
{

    public function login()
    {
      Pages::login();
    }

    public function logout()
    {
        Pages::logout();
    }

    public function post()
    {
        Pages::post();
    }

    public function home()
    {
        Pages::home();
    }

    public function changePassword()
    {
        Pages::changePassword();
    }

    public function changePost()
    {
        if (isset($_SESSION['USER']) && isset($_GET['id']) &&
            unserialize($_SESSION['USER'])->isSuperUser()) {

            $id = $_GET['id'];
            Pages::changePost($id);
        }
    }

    public function changeComment()
    {
        if(isset($_SESSION['USER'])) {
            $id = $_GET['id'];
            Pages::changeComment($id);
        }
    }

}
