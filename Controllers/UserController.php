<?php

namespace Blog\Controllers;

use \Blog\Models\User;

class UserController
{

    public function changePassword()
    {
        if(isset($_POST['submit'])) {

            $password = $_POST['password'];
            $confirmPassword = $_POST['confirm-password'];
            $currentPassword = $_POST['current-password'];
            $email = $_SESSION['USER']->getEmail();

            if($password != $confirmPassword) {
                echo "Пароли отличаються";
                return;
            }

            if(User::changePassword($email, $password, $currentPassword)) {
                $message = "Пароль изменен";
                require __DIR__ ."/../Views/success.php";
            } else {
                echo "Произошла ошибка";
            }
        }
    }

    public function profile()
    {
        $email = $_GET['email'];
        $user = User::get($email);
        require_once __DIR__ . "/../Views/user/profile.php";
    }

    public function exit()
    {
        User::exit();
    }
}
