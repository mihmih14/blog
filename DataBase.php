<?php

namespace Blog;

class DataBase
{

    private static $db;

    private static $HOST = "localhost";

    private static $DB_NAME = "blog";

    private static $ROOT = "root";

    private static $PASSWORD = "MihMihAdmin14";

    private function __construct() {}

    private function __clone() {}

    public static function getInstance()
    {

        if(!isset(self::$db)) {
          self::$db = new \PDO(
              "mysql:host=".self::$HOST.";dbname=".self::$DB_NAME.";charset=utf8mb4",
              self::$ROOT,
              self::$PASSWORD,
              [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]);
        }

        return self::$db;
    }

}

